#include <sstream>

class sequence_diagram
{
public:
	sequence_diagram(const char* title)
	{
		data << "Title : " << title << "\\n";
	}

	void say(const char* from, const char* to, const char* message)
	{
		data << from << "->" << to << ": " << message << "\\n";
	}

	// 0 over -1 left 1 right
	void note(const char* node, int pos, const char* message)
	{
		const char* pos_str = NULL;
		switch (pos)
		{
		case -1:
			pos_str = "Note left of ";
			break;
		case 1:
			pos_str = "Note right of ";
			break;
		default:
			pos_str = "Note over ";
			break;
		}

		data << pos_str << node << ": " << message << "\\n";
	}

	int save(const char* file_name)
	{
		FILE* file = fopen(file_name, "wb");
		if (!file)
			return false;
		fprintf(file, html_template, data.str().c_str());
		fclose(file);
		return true;
	}

private:
	std::stringstream data;
	const char* html_template = R"(<html>  
  <body>
    <script src="https://bramp.github.io/js-sequence-diagrams/js/webfont.js"></script>
    <script src="https://bramp.github.io/js-sequence-diagrams/js/snap.svg-min.js"></script>
    <script src="https://bramp.github.io/js-sequence-diagrams/js/underscore-min.js"></script>
    <script src="https://bramp.github.io/js-sequence-diagrams/js/sequence-diagram-min.js"></script>

    <div id="diagram"></div>
    <script>
      var diagram = Diagram.parse("%s");
      //diagram.drawSVG("diagram", {theme: 'hand'});
      diagram.drawSVG("diagram", {theme: 'simple'});
    </script>
 </body>
</html>)";
};
