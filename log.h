inline void log_string(const char* format, ...)
{
	static FILE* file = NULL;
	if (!file)
	{
		const char* name = "log_string.txt";
		file = fopen(name, "a");
		if (!file)
			return;
	}

	va_list args;
	va_start (args, format);
	vfprintf (file, format, args);
	va_end (args);
}

